import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn.model_selection import KFold, StratifiedKFold
import lightgbm as lgb
from lightgbm import LGBMRegressor
from glob import glob
from sklearn.metrics import mean_squared_error
import numpy as np
from sklearn.preprocessing import MultiLabelBinarizer
from bayes_opt import BayesianOptimization

path_train = 'datacsv/k62/train/*.csv'
train_files = glob(path_train)

path_test = "datacsv/k62/test/*.csv"
test_files = glob(path_test)


def concat_dataset(files):
    li = []
    for filename in files:
        df = pd.read_csv(filename, index_col=None, header=0)
        li.append(df)
    dataFrame = pd.concat(li, axis=0, ignore_index=True)
    return dataFrame


def bayes_parameter_opt_lgb(X, y, init_round=20, opt_round=30, n_folds=5, random_seed=6, n_estimators=10000,
                            learning_rate=0.05, output_process=False):
    # prepare data

    train_data = lgb.Dataset(data=X, label=y)
    # parameters

    def lgb_eval(num_leaves, feature_fraction, bagging_fraction, max_depth,
                 lambda_l1, lambda_l2, min_split_gain, min_child_weight):

        params = {'objective': 'regression', 'num_iterations': 1000, 'learning_rate': 0.05,
                  'early_stopping_round': 100, 'metric': 'rmse'}
        params["num_leaves"] = int(round(num_leaves))
        params['feature_fraction'] = max(min(feature_fraction, 1), 0)
        params['bagging_fraction'] = max(min(bagging_fraction, 1), 0)
        params['max_depth'] = int(round(max_depth))
        params['lambda_l1'] = max(lambda_l1, 0)
        params['lambda_l2'] = max(lambda_l2, 0)
        params['min_split_gain'] = min_split_gain
        params['min_child_weight'] = min_child_weight

        cv_result = lgb.cv(params, train_data, nfold=3, seed=random_seed,
                           stratified=False, verbose_eval=200, metrics=['rmse'])

        return min(cv_result['rmse-mean'])

    # setting range of the parameters
    lgbBO = BayesianOptimization(lgb_eval, {'num_leaves': (24, 128),
                                            'feature_fraction': (0.1, 0.9),
                                            'bagging_fraction': (0.5, 1),
                                            'max_depth': (5, 10),
                                            'lambda_l1': (0, 5),
                                            'lambda_l2': (0, 3),
                                            'min_split_gain': (0.001, 0.1),
                                            'min_child_weight': (5, 60)}, random_state=0)
    # optimize
    lgbBO.maximize(init_points=init_round, n_iter=opt_round)

    # output optimization process
    if output_process == True:
        lgbBO.points_to_csv("bayes_opt_result.csv")

    # return
    return lgbBO


def mean_absolute_percentage_error(y_true, y_pred):
    """Calculates MAPE given y_true and y_pred"""
    y_pred = np.array(y_pred)
    y_true = np.array(y_true)
    return np.mean(np.abs((y_true - y_pred) / y_true)) * 100


if __name__ == "__main__":
    df_train = concat_dataset(train_files)
    df_test = concat_dataset(test_files)
    features = ["XT", "AC"]
    label = "TOTAL_AC"
    # params = bayes_parameter_opt_lgb(df_train[features], df_train[label], init_round=5, opt_round=10, n_folds=5,
    #                                  random_seed=6, n_estimators=10000, learning_rate=0.05)
    # params = params.max['params']
    # print(params)
    oof_preds = np.zeros(df_train.shape[0])
    sub_preds = np.zeros(df_test.shape[0])
    stratified = False
    num_folds = 5
    # hyper_params = {
    #     "objective": "regression",
    #     "metric": "rmse",
    #     "bagging_frequency": 5,
    #     "verbosity": -1,

    #     # Selected rounded-off params
    #     'bagging_fraction': 0.5,
    #     'feature_fraction': 0.8,
    #     'lambda_l1': 4,
    #     'lambda_l2': 2.6,
    #     'max_depth': 10,
    #     'min_child_weight': 49,
    #     'min_split_gain': 0.05,
    #     'num_leaves': 105
    # }
    # params = {
    #     "objective": "regression",
    #     "metric": "rmse",
    #     "bagging_frequency": 5,
    #     "verbosity": -1,

    #     # Selected rounded-off params
    #     'bagging_fraction': 0.6,
    #     'feature_fraction': 0.7,
    #     'lambda_l1': 3,
    #     'lambda_l2': 2.5,
    #     'max_depth': 9,
    #     'min_child_weight': 49,
    #     'min_split_gain': 0,
    #     'num_leaves': 102
    # }
    params = {"objective": "regression",
              "metric": "rmse",
              "bagging_frequency": 5,
              "bagging_seed": 2018,
              "verbosity": 0,

              # Selected rounded-off params
              'bagging_fraction': 0.7,
              'feature_fraction': 1,
              'lambda_l1': 1,
              'lambda_l2': 0,
              'max_depth': 10,
              'min_child_weight': 20,
              'min_split_gain': 0,
              'num_leaves': 24}
    if stratified:
        folds = StratifiedKFold(
            n_splits=num_folds, shuffle=True, random_state=50)
    else:
        folds = KFold(n_splits=num_folds, shuffle=True, random_state=50)
    # features = ["AT", "XT", "AC"]
    # label = "TOTAL_AC"
    for n_fold, (train_idx, valid_idx) in enumerate(folds.split(df_train[features], df_train[label])):
        train_x, train_y = df_train[features].iloc[train_idx], df_train[label].iloc[train_idx]
        valid_x, valid_y = df_train[features].iloc[valid_idx], df_train[label].iloc[valid_idx]
        print("train_x", train_x)
        print("valid_x", valid_x)

        gbm = LGBMRegressor(**params)

        gbm.fit(train_x, train_y, eval_set=[(train_x, train_y), (valid_x, valid_y)],
                early_stopping_rounds=100)

        oof_preds[valid_idx] = gbm.predict(
            valid_x, num_iteration=gbm.best_iteration_)
        sub_preds += gbm.predict(df_test[features],
                                 num_iteration=gbm.best_iteration_) / folds.n_splits
        print('Fold %2d MSE : %.6f' %
              (n_fold + 1, mean_squared_error(valid_y, oof_preds[valid_idx])))
        del gbm, train_x, train_y, valid_x, valid_y
    print('Full MSE score %.6f' %
          mean_squared_error(df_train[label], oof_preds))

    df_test['TOTAL_AC_PREDICT'] = sub_preds

    print(df_test[label], abs(df_test['TOTAL_AC_PREDICT']))
    print('Full MSE score %.6f' %
          mean_squared_error(df_test[label], abs(df_test['TOTAL_AC_PREDICT'])))
    mape = mean_absolute_percentage_error(
        df_test[label], abs(df_test['TOTAL_AC_PREDICT']))
    print("Độ lệch: {}".format(100-mape))

    # Thông tin test
    y_test_ward = np.concatenate((sub_preds, np.array([0])))
    count_test = 0
    len_of_test = np.array([])
    tmp = 1
    for element in range(len(y_test_ward) - 1):
        if y_test_ward[element] != y_test_ward[element + 1]:
            count_test = count_test + 1
            len_of_test = np.concatenate((len_of_test, np.array([tmp])))
            tmp = 1
        else:
            tmp = tmp + 1
    start_of_test = np.array([0])
    for element in range(1, count_test):
        start_of_test = np.concatenate((start_of_test, np.array(
            [len_of_test[element - 1] + start_of_test[element - 1]])))
    import math

    def mean_absolute_percentage_error_25(y_true, y_pred):
        y_true = np.array(y_true)
        y_pred = np.array(y_pred)

        y_true_25 = np.array([])
        y_pred_25 = np.array([])
        for element in range(count_test):
            t = start_of_test[element] + math.floor(len_of_test[element] / 4)
            y_true_25 = np.concatenate(
                (y_true_25, np.array([y_true[int(t.item())]])))
            y_pred_25 = np.concatenate(
                (y_pred_25, np.array([y_pred[int(t.item())]])))
        return np.mean(np.abs((y_true_25 - y_pred_25) / y_true_25)) * 100

    # Tương tự với 25%, khác ở chỗ thay vì chỉ lấy 1 lần báo cáo ở thời điểm 25%, ta lấy từ đầu tới thời điểm đó

    def mean_absolute_percentage_error_1_to_25(y_true, y_pred):
        y_true = np.array(y_true)
        y_pred = np.array(y_pred)

        y_true_1_to_25 = np.array([])
        y_pred_1_to_25 = np.array([])
        for element in range(count_test):
            t = start_of_test[element] + math.floor(len_of_test[element] / 4)
            for t_value in range(int(start_of_test[element].item()), int(t.item())):
                # Chọn vòng for dưới này khi ta muốn lấy từ đầu tới thời điểm vừa mới vượt quá 25%
                # Ưu điểm: Lấy được đầu vào với những dự án không quá 3 lần báo cáo
                # for t_value in range( int(start_of_test[element].item()), int(t.item()) + 1 ):
                y_true_1_to_25 = np.concatenate(
                    (y_true_1_to_25, np.array([y_true[t_value]])))
                y_pred_1_to_25 = np.concatenate(
                    (y_pred_1_to_25, np.array([y_pred[t_value]])))
    # print('y_true:')
    # print(y_true)
    # print('y_true_1_to_25:')
    # print(y_true_1_to_25)
    # print('y_pred:')
    # print(y_pred)
    # print('y_pred_1_to_25:')
    # print(y_pred_1_to_25)
        return np.mean(np.abs((y_true_1_to_25 - y_pred_1_to_25) / y_true_1_to_25)) * 100

# 50%
# Tương tự 25%
    def mean_absolute_percentage_error_50(y_true, y_pred):
        y_true = np.array(y_true)
        y_pred = np.array(y_pred)

        y_true_50 = np.array([])
        y_pred_50 = np.array([])
        for element in range(count_test):
            t = start_of_test[element] + math.floor(len_of_test[element] / 2)
            y_true_50 = np.concatenate(
                (y_true_50, np.array([y_true[int(t.item())]])))
            y_pred_50 = np.concatenate(
                (y_pred_50, np.array([y_pred[int(t.item())]])))
    # print('y_true:')
    # print(y_true)
    # print('y_true_50:')
    # print(y_true_50)
    # print('y_pred:')
    # print(y_pred)
    # print('y_pred_50:')
    # print(y_pred_50)
        return np.mean(np.abs((y_true_50 - y_pred_50) / y_true_50)) * 100

# 1->50%
# Tương tự 1->25%
    def mean_absolute_percentage_error_1_to_50(y_true, y_pred):
        y_true = np.array(y_true)
        y_pred = np.array(y_pred)

        y_true_1_to_50 = np.array([])
        y_pred_1_to_50 = np.array([])
        for element in range(count_test):
            t = start_of_test[element] + math.floor(len_of_test[element] / 2)
            for t_value in range(int(start_of_test[element].item()), int(t.item())):
                y_true_1_to_50 = np.concatenate(
                    (y_true_1_to_50, np.array([y_true[t_value]])))
                y_pred_1_to_50 = np.concatenate(
                    (y_pred_1_to_50, np.array([y_pred[t_value]])))

        return np.mean(np.abs((y_true_1_to_50 - y_pred_1_to_50) / y_true_1_to_50)) * 100
    mape = mean_absolute_percentage_error(df_test[label], abs(sub_preds))
    mape25 = mean_absolute_percentage_error_25(df_test[label], abs(sub_preds))
    mape1to25 = mean_absolute_percentage_error_1_to_25(
        df_test[label], abs(sub_preds))
    mape50 = mean_absolute_percentage_error_50(df_test[label], abs(sub_preds))
    mape1to50 = mean_absolute_percentage_error_1_to_50(
        df_test[label], abs(sub_preds))

    print("Độ chính xác trung bình của dữ liệu K61:")
    print('Tất cả thời điểm:')
    print(100 - mape)
    print('25%:')
    print(100 - mape25)
    print('1 -> 25%:')
    print(100 - mape1to25)
    print('50%:')
    print(100 - mape50)
    print('1 -> 50%:')
    print(100 - mape1to50)
    plt.plot(df_test[label], 'o', color='r')
    plt.plot(abs(df_test['TOTAL_AC_PREDICT']), 'x', color='b')
    plt.title("Mô hình K61-KSTN:")
    plt.xlabel("STT")
    plt.ylabel("Cost")

    plt.legend(('prediction', 'reality'), loc='upper right')

    plt.show()

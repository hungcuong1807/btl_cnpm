from flask import Flask
from twilio.twiml.voice_response import VoiceResponse

app = Flask(__name__)


@app.route("/record", methods=['GET', 'POST'])
def record():
    print("hello world")
    """Returns TwiML which prompts the caller to record a message"""
    # Start our TwiML response
    response = VoiceResponse()

    # Use <Say> to give the caller some instructions
    response.say("Hello, I am Cuong. I am twenty years old")

    # Use <Record> to record the caller's message
    response.record(timeout=0, transcribe=True)

    # End the call with <Hangup>
    response.hangup()
    print(str(response))

    return str(response)


@app.route("/", methods=['GET'])
def hello():
    return "hello world"


if __name__ == "__main__":
    app.run(port=5000)
